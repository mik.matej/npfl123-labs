task5
---
* dialogs	1000
* turns	12936
* system sentences	18340
* user words	61506
* system words	119440
* turns per dialog mean	12.936
* turns per dialog dev	1.6498193840539102
* system sentences per turn mean	1.4177489177489178
* system sentences per turn dev	0.6307472211060193
* user words per turn mean	4.754638218923933
* user words per turn dev	3.254809743235657
* system words per turn mean	9.233147804576376
* system words per turn dev	5.571378803067463
* user vocabulary size	84
* user entropy	4.130010560588157
* user conditional entropy	1.1024782007041114
* system vocabulary size	959
* system entropy	4.2639019266575096
* system conditional entropy	0.6667618006717528

task6
---
* dialogs	1618
* turns	12140
* system sentences	14404
* user words	41357
* system words	145054
* turns per dialog mean	7.503090234857849
* turns per dialog dev	2.5644335147235546
* system sentences per turn mean	1.186490939044481
* system sentences per turn dev	0.433348758079185
* user words per turn mean	3.40667215815486
* user words per turn dev	3.0010879350117246
* system words per turn mean	11.948434925864909
* system words per turn dev	8.70402830608787
* user vocabulary size	522
* user entropy	4.320000991913518
* user conditional entropy	1.6410883102583393
* system vocabulary size	573
* system entropy	4.549932393879158
* system conditional entropy	1.1854062931589282

comments
---
The user in task5 only uses a few words every turn and they repeat a lot, so he has low vocabulary size.
The system in task5 has the biggest vocabulary and the lowest conditional entropy (there are many words, so each appears only in a few different bigrams).
The user and system in task6 have similar vocabulary sizes, but the system has longer turns, so its conditional entropy is also lower.