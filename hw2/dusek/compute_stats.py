#!/usr/bin/env python3

from argparse import ArgumentParser
from collections import Counter
import re
import numpy as np

def get_stats(dialogues, party, stats_func):
    return [stats_func(turn[party]) for dialogue in dialogues for turn in dialogue]


def process_file(filename):

    dialogues = []
    dialogue = []
    with open(filename, 'r', encoding='UTF-8') as fh:
        for line in fh:
            line = line.strip()
            if not line:  # start of new dialogue
                dialogues.append(dialogue)
                dialogue = []
            if '\t' not in line:  # skip search results
                continue
            usr, sys = line.split('\t')
            usr = re.sub('^[0-9]+ ', '', usr)
            if usr == '<SILENCE>':
                # TODO remove api_call
                if dialogue:
                    dialogue[-1]['sys'] += '\t' + sys  # join multi-line statements with a tab (will be a sentence boundary)
                else:
                    usr = ''  # ignore silence at dialogue start
            else:
                dialogue.append({'usr': usr, 'sys': sys})

    print(f'Number of dialogues: %d' % len(dialogues))
    num_turns = [len(d) for d in dialogues]
    print('Number of turns: %d, mean: %.3f, std: %.3f' % (sum(num_turns), np.mean(num_turns), np.std(num_turns)))
    for party in 'usr', 'sys':
        # number of sentences -- sentence splitting on ., ?, ! and tab (multi-line)
        num_sents = get_stats(dialogues, party, lambda t: len([s for s in re.split('[!?\.\t]', t) if s]))
        print('%s -- Number of sentences: %d, per turn mean: %.3f, std: %.3f' %
              (party, sum(num_sents), np.mean(num_sents), np.std(num_sents)))
        # number of words -- split on whitespace
        num_words = get_stats(dialogues, party, lambda t: len([w for w in re.split('\s+', t) if t]))
        print('%s -- Number of words: %d, per turn mean: %.3f, std: %.3f' %
              (party, sum(num_words), np.mean(num_words), np.std(num_words)))
        # using Counters to get word frequencies for each turn, summing them gets global word frequencies
        vocab = sum(get_stats(dialogues, party, lambda t: Counter(re.split('\s+', t))), Counter())
        print('%s -- Vocabulary size: %d' % (party, len(vocab)))
        # Shannon entropy
        text_len = sum(num_words)
        entropy = - sum([freq/text_len * np.log2(freq/text_len) for freq in vocab.values()])
        print('%s -- Entropy: %.3f' % (party, entropy))
        # conditional entropy -- need to collect bigram frequencies (the zip... statement produces bigrams)
        bigrams = sum(get_stats(dialogues, party, lambda t: Counter(zip(re.split('\s+', t), re.split('\s+', t)[1:]))), Counter())
        cond_entropy = - sum([freq/text_len * np.log2(freq/vocab[c]) for (c, w), freq in bigrams.items()])
        print('%s -- Bigram conditional: %.3f' % (party, cond_entropy))


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('filename', type=str, help='Filename to parse')
    args = ap.parse_args()
    process_file(args.filename)
