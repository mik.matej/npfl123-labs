# NPFL123 Labs Homework 2, deadline: March 15th

The task in this lab is to explore dialogue datasets and find out more about them.
 Your job will thus be to write a script that computes some basic statistics about datasets,
 and then try to interpret the script's results.

## Requirements

1. Download the [bAbI 6 Dialogue Tasks](https://research.fb.com/downloads/babi/) dataset. 
 Read the description of the data format on the website. You'll be working with **Tasks 5 and 6**
 (containing full generated dialogues and [DSTC2](http://camdial.org/~mh521/dstc/) data).

2. Write a script that will **read all turns in the data and separate the user and system utterances** in the training set. 
    * Make the script ignore any search results lines in the data (they don't contain a tab character).
    * If the script finds a turn where the user is silent (the user turn contains only `<SILENCE>`), it should concatenate the 
      system response from this turn to the previous turn. Note that this may happen on multiple consecutive turns, 
      and the script should join all of these together into one system response.

3. Implement a routine that will **compute the following statistics** for both bAbI tasks for system and user turns:
    * data length (total number of dialogues, turns, sentences, words)
    * mean and standard deviations for individual dialogue lengths (number of turns in a dialogue, number of sentences and words in a turn)
    * vocabulary size
    * Shannon entropy and bigram conditional entropy (see lecture 2 slides)

4. Along with your script, submit also a **printout of the results along with your own comments**,
  comparing the results between the two bAbI Tasks. 3-5 sentences is enough, but try to explain
  why you think the vocabulary and entropy numbers are different.


Note that you don't need to submit the data (not even with filtered/joined system turns) – just the script, 
the printout of the statistics, and your explanation.