# NPFL123-labs

This repository contains the pages with lab homeworks for the [UFAL Dialogue Systems course](http://ufal.cz/npfl123), 2020 run.

The individual homework assignments will be listed on separate webpages below. The *deadline* for each homework is 12 days from the day it was assigned (i.e., Sunday next week), 23:59 CET. Additional homework will be assigned for missed deadlines.

To turn in your homework, create a pull request with a subdirectory with your name under the respective directory (e.g., [hw1](hw1/)). You can commit just a file containing a link to your own repo if you like/need to. The preferred format is Markdown for texts and Python for code, plus anything you need for other types of data.

Since this is basically the first run of this type of lab exercises, everything may change in the meantime, but you'll be kept informed.

### Assignments

* [Lab 1: Mockup – Feb 18](hw1/README.md) -- deadline: Mar 1
* There will be no labs in the 2nd week of the semester
* [Lab 2: Data exploration – Mar 3](hw2/README.md) -- deadline: Mar 15
* [Lab 3: Handcrafted NLU – Mar 10](hw3/README.md) -- deadline: Mar 22
* [Lab 4: Statistical NLU – Mar 17](hw4/README.md) -- deadline: Mar 29
* [Lab 5: State tracking – Mar 24](hw5/README.md) -- deadline: Apr 5
* [Lab 6: Policy – Mar 31](hw6/README.md) -- deadline: Apr 12
* [Lab 7: Backend calls – Apr 7](hw7/README.md) -- deadline: Apr 19
* [Lab 8: NLG – Apr 14](hw8/README.md) -- deadline: Apr 26
* [Lab 9: Integration – Apr 22](hw9/README.md) -- deadline: May 3
* [Lab 10: ASR – Apr 27](hw10/README.md) -- deadline: May 10
* [Lab 11: TTS preprocessing – May 5](hw11/README.md) -- deadline: May 17
* [Lab 12: IR chatbot – May 12](hw12/README.md) -- deadline: May 24

