
# NPFL123 Labs Homework 5, deadline: April 5th

This week, you will build a simple probabilistic _dialogue/belief state tracker_ for both your own domain of choice and DSTC2.

## What you should do

1. Implement a dialogue state tracker that works with the `dial['state']` structure and fills it with a probability distribution
   of values over each slot (assume slots are independent), updated during the dialogue. Don't forget `None` is a valid value, meaning 
   “we don't know/user didn't say anything about this”. 
   
   At the beginning of the dialogue, each slot should be initialized with the distribution `{None: 1.0}`.

   The update rule for a slot, say `food`, should go like this:
   * Take all mentions of `food` in the current NLU, with their probabilities. Say you got `Chinese` with a probability of 0.7 and 
     `Italian` with a probability of 0.2. This means `None` has a probability of 0.1.
   * Use the probability of `None` to multiply current values with it (e.g. if the distribution was `{'Chinese': 0.2, None: 0.8}`, 
     it should be changed to `{'Chinese': 0.02, None: 0.08}`.
   * Now add the non-null values with their respective probabilities from the NLU. This should result in 
     `{'Chinese': 0.72, 'Italian': 0.2, None: 0.08}`.

2. Add the tracker into configuration files both for your own rule-based domain and for DSTC2. In addition, 
   replace `dialmonkey.policy.dummy.ReplyWithNLU`  with `dialmonkey.policy.dummy.ReplyWithState`.

3. Run your NLU + tracker over your NLU examples from [../hw3](homework 3) and the first 20 lines from the [DSTC2 development data](../hw4/dstc2-nlu-dev.json)
   and save the outputs to a text file.


## What you should submit

* Commit your tracker in the Dialmonkey repository under `dialmonkey.dst.rule_<your-name>`. 
* Commit your updated configuration files for both domains into the Dialmonkey repository.
* Commit your text file with the outputs here into this directory.
   
