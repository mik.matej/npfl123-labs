import tensorflow as tf
import numpy as np
import json
from dialmonkey.da import DA

from collections import Counter

MIN_WORD_COUNT = 2 #join all words with less appearances in train data into the last position in bag of words representation
HIDDEN_LAYER = 250

def to_bag(sequence, categories):
    bag = np.zeros(len(categories) + 1)
    for token in sequence:
        try:
            bag[categories.index(token)] += 1
        except ValueError:
            bag[len(categories)] += 1
    return bag

def format_data(data, words, DAs):
    formated = {
        'in': np.zeros((len(data), len(words) + 1)),
        'out': np.zeros((len(data), len(DAs) + 1))
    }
    for i, turn in enumerate(data):
        formated['in'][i] = to_bag(turn['usr'].split(), words)
        formated['out'][i] = to_bag(DA.parse_cambridge_da(turn['DA']).dais, DAs)
    return formated

data = json.load(open('dstc2-nlu-train.json'))

#TODO maybe create 'words' and 'DAs' simultaneusly
word_counter = Counter([token for turn in data for token in turn['usr'].split()])
words = [word for word in word_counter if word_counter[word] >= MIN_WORD_COUNT]
DAs = list(set([item for turn in data for item in DA.parse_cambridge_da(turn['DA']).dais]))

train = format_data(data, words, DAs)
dev = format_data(json.load(open('dstc2-nlu-dev.json')), words, DAs)
model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(len(words) + 1),
    tf.keras.layers.Dense(HIDDEN_LAYER, activation=tf.nn.relu),
    tf.keras.layers.Dense(len(DAs) + 1, activation=tf.nn.sigmoid)
])
model.summary()
model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.binary_crossentropy,
    metrics=[
        tf.keras.metrics.TruePositives(),
        tf.keras.metrics.FalsePositives(),
        tf.keras.metrics.FalseNegatives()
    ]
)
model.fit(
    train['in'],
    train['out'],
    batch_size=50,
    epochs=30
)

model.evaluate(
    dev['in'],
    dev['out']
)

model.save('NLU_model')

with open('words.txt', mode='w') as file:
    for word in words:
        print(word, file=file)
with open('DAs.txt', mode='w') as file:
    for DA in DAs:
        print(DA, file=file)