# NPFL123 Labs Homework 6, deadline: April 12th

This week, you will build a rule-based policy for your domain.

## What you should do

1. Implement a rule-based policy that uses the current NLU intent (or intents) and the
   dialogue state probability distribution to produce system action dialogue acts.
   You can use just the most probable value from each state, assuming its probability is higher
   than a certain threshold (e.g. 0.7).

   The policy should:
   * Check the current intent(s) and split the action according to that
   * Given the intent, check that the state contains all necessary slots to respond:
   
       * if it does, fill in a response system DA into the `dial.action` field.
       * if it doesn't, fill in a system DA requesting more information into the `dial.action` field.

   Use the flowcharts you built in [HW1](../hw1) to guide the policy's decisions.

   For now, skip any API queries and hardcode the responses (you will build the actual 
   backend a week later).

2. Save the policy under `dialmonkey.policy.<your_domain>` and add it into the
   configuration file for your own domain.
   
3. In your configuration file, replace `dialmonkey.policy.dummy.ReplyWithState` 
   with `dialmonkey.policy.dummy.ReplyWithSystemAction`
   (don't worry that you have two things from the policy package in your pipeline at the moment).

3. Check that your policy returns reasonable system actions for each of your NLU test utterances,
   if you treat that utterance as a start of a dialogue.


## What you should submit

* Commit your policy and your updated configuration file in the Dialmonkey repository.
   
