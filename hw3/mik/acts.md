# DAs for basketball domain
* request_stats
  * type = team, player
  * name = (open)
  * stat = points, 2pt_made, 3pt_made, ft_made, ft_percentage, wins
  * rank = (open)
  * value = (open)
* request_game
  * team = (open)
  * time = (open)
  * place = (open)
* set
  * default
  * season = (open)
  * team = (open)