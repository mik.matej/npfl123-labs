Kuchařka
===

1.
* **U:** Mám chuť na kuskus s tuňákem.
* **S:** Nalezeno *43* receptů. Chcete recept *Kuskus s tuňákem snadno a rychle!* s nejlepším hodnocením?
* **U:** Ano.
* **S:** Potřebné ingredience jsou: *kuskus - 100g, plechovka tuňák ve vlastní šťávě*.
* **U:** Chci začít vařit.
* **S:** Pro přípravu kuskusu můžete použít *rychlovarnou konvici* nebo *sporák*. 
* **U:** Chci použít konvici.
* **S:** V rychlovarné konvici uvařte *0.2l* vody.
* **U:** Voda už je vroucí.
* **S:** Zalijte kuskus a nechte 3 minuty odstát.
* **U:** Kuskus je hotový.
* **S:** Přidejte tuňáka z plechovky a konzumujte.

2.
* **U:** Doporuč mi recept.
* **S:** Myslím, že se vám bude líbit recept *žabí stehýnka s brambory*, protože má vysoké hodnocení.
* **U:** Obsahuje med?
* **S:** Recept *žabí stehýnka s brambory* nemá v ingrediencích *med*.
* **U:** Obsahuje žabí stehýnka?
* **S:** Recept *žabí stehýnka s brambory* má v ingrediencích položku *žabí stehýnka - 200g*.
* **U:** Doporuč mi jiný recept.
* **S:** Myslím, že se vám bude líbit recept *těstovinový salát s tuňákem*, protože jste nedávno vařil *Kuskus s tuňákem snadno a rychle!*.
* **U:** Jak dlouho trvá příprava?
* **S:** Čas na přípravu receptu *těstovinový salát s tuňákem* je *40 minut*.
* **U:** Přidej ho do oblíbených receptů.
* **S:** Recept *těstovinový salát s tuňákem* uložen do seznamu *oblíbené recepty*.

3.
* **U:** Chci hledat recept.
* **S:** Chcete hledat v uložených seznamech nebo v internetové databázi?
* **U:** Na internetu.
* **S:** Zadejte parametry vyhledávání v internetové databázi.
* **U:** Délka přípravy menší než jedna hodina a recept obsahuje vepřové maso.
* **S:** Přidány parametry *doba přípravy - <1h*, *ingredience - vepřové maso*. Zadejte další parametry nebo hledejte. 
* **U:** Ještě ať obsahuje cibuli.
* **S:** Přidán parametr *ingredience - cibule*. Zadejte další parametry nebo hledejte.
* **U:** Hledej guláš.
* **S:** Vyhledávám *guláš* s parametry *doba přípravy - <1h*, *ingredience - vepřové maso, cibule*. Nalezeno *6* výsledků.
* **U:** Přečti výsledky.
* **S:** *Lahodný gulášek z kotlíku*.
* **U:** Další.
* **S:** *Guláš s chilli a brambory*.
* **U:** Ulož do seznamu *experimenty*.
* **S:** Recept *Guláš s chilli a brambory* uložen do seznamu *experimenty*.
4.
* **U:** Chci si připravit ovocný salát z mojich oblíbených receptů.
* **S:** Potřebné ingredience jsou *jablko - 2ks, hruška - 1ks, banán - 2ks*.
* **U:** Ok, to všechno mám.
* **S:** Umyjte jablka a hrušku.
* **U:** Umyl jsem jablka.
* **S:** Ještě umyjte hrušku.
* **U:** Umyl jsem všechno.
* **S:** Oloupejte banány a vše nakrájejte na kostičky.
* **U:** Oloupal jsem banány a nakrájel je na kostičky.
* **S:** Ještě nakrájejte jablka a hrušku na kostičky.
* **U:** Nakrájel jsem i jablka a hrušku na kostičy.
* **S:** Nasypte vše do mísy a podávejte.
* **U:** Chci odstranit tento recept z oblíbených.
* **S:** Recept *Ovocný salát* odstraněn z oblíbených receptů.

5.
* **U:** Chci vytvořit nový recept.
* **S:** Vytvářím nový recept. Jak se bude jmenovat?
* **U:** Zmrzlinový sen.
* **S:** Název receptu je *Zmrzlinový sen*. Zadejte ingredience.
* **U:** Voda.
* **S:** Jaké množství ingredience voda?
* **U:** Jedno deci.
* **S:** Přidáno *voda - 0.1l*. Chcete přidat další ingredience?
* **U:** Přidej ještě dvě lžíce cukru.
* **S:** Přidáno *cukr - 2 lžíce*. Chcete přidat další ingredience?
* **U:** To je všechno.
* **S:** Zadejte *1.* krok postupu.
* **U:** Rozmíchej cukr ve vodě.
* **S:** Zadejte *2.* krok postupu.
* **U:** Nech vodu ztuhnout v mrazáku.
* **S:** Zadejte *3.* krok postupu.
* **U:** To už je všechno.
* **S:** Recept *Zmrzlinový sen*  do seznamu *Moje recepty*.